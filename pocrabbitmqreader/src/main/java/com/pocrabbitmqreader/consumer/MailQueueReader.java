package com.pocrabbitmqreader.consumer;

import com.pocrabbitmqreader.model.MailMessage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import java.io.IOException;

@Component @AllArgsConstructor @Slf4j
public class MailQueueReader {

    @RabbitListener(queues = "MAIL_CELIM" )
    public void sendLoanConfirmation( MailMessage message ) throws IOException {
        log.info("Received MAIL_CELIM <" + message.toString() + ">");
    }

}
