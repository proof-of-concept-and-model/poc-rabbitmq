package com.pocrabbitmq.publisher;

import com.pocrabbitmq.model.MailMessage;
import lombok.AllArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

@Component @AllArgsConstructor
public class MailPublisher {

    private final AmqpTemplate amqpTemplate;

    public void publishMail(MailMessage mailMessage ) {
        amqpTemplate.convertAndSend("mail_topic","mail_key", mailMessage);
    }


}
