package com.pocrabbitmq.runner;

import com.pocrabbitmq.model.MailMessage;
import com.pocrabbitmq.service.MailService;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.util.Date;

@Component @AllArgsConstructor
public class MailCreator implements CommandLineRunner {

    private final MailService mailService;

    @Override
    public void run(String... args) throws Exception {

        for ( int i = 0; i < 10; i++ ) {
            MailMessage mailMessage = MailMessage.builder()
                    .from("Expéditeur : " + i)
                    .to("Destinataire : " + i)
                    .sentDate(new Date())
                    .subject("Sujet : " + i)
                    .text("Test message : " + i)
                    .build();

            mailService.publishMailToQueue(mailMessage);
        }
    }

}
