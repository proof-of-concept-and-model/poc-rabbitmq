package com.pocrabbitmq.service;

import com.pocrabbitmq.model.MailMessage;
import com.pocrabbitmq.publisher.MailPublisher;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service @AllArgsConstructor
public class MailService {

    private final MailPublisher mailPublisher;

    public void publishMailToQueue(final MailMessage mailMessage) {

        MailMessage mailMessageToPublish = MailMessage.builder()
                .from(mailMessage.getFrom())
                .replyTo(mailMessage.getReplyTo())
                .to(mailMessage.getTo())
                .cc(mailMessage.getCc())
                .bcc(mailMessage.getBcc())
                .sentDate(mailMessage.getSentDate())
                .subject(mailMessage.getSubject())
                .text(mailMessage.getText())
                .build();

        mailPublisher.publishMail(mailMessageToPublish);
    }

}
