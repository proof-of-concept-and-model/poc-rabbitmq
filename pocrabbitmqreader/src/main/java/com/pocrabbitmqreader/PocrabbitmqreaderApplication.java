package com.pocrabbitmqreader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocrabbitmqreaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocrabbitmqreaderApplication.class, args);
    }

}
