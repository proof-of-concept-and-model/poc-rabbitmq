package com.pocrabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocrabbitmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocrabbitmqApplication.class, args);
    }

}
